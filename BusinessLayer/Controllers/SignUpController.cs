﻿using System.Web.Helpers;
using EntityLayer.Models;
using DataLayer.UnitOfWork;

namespace BusinessLayer.Controllers
{
    public class SignUpController
    {
        private readonly IUnitOfWork _unitOfWork;

        public SignUpController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<bool> CreateAccount(User user)
        {
            if (!(await CheckEmail(user.Email)))
                return false;

            var data = new User
            {
                Email = user.Email,
                Password = Crypto.HashPassword(user.Password),
                Age = user.Age,
                Country = user.Country,
                FirstName = user.FirstName,
                LastName = user.LastName,
                University = user.University,
                ScienceArea = user.ScienceArea,
                Picture = user.Picture,
                Created = DateTime.Now
            };

            _unitOfWork.User.Add(data);
            _unitOfWork.Complete();

            return true;
        }

        private async Task<bool> CheckEmail(string email)
        {
            return !await _unitOfWork.User.Find(x => x.Email.Equals(email)).AnyAsync();
        }
    }
}

using EntityLayer.Models;
using Microsoft.EntityFrameworkCore;
using DataLayer.UnitOfWork;

namespace BusinessLayer.Controllers
{
    public class UserController
    {
        private readonly IUnitOfWork _unitOfWork;

        public UserController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<User>> GetSuggestions(string email, string originalEmail)
        {
            var user = await _unitOfWork.User.GetAll().Where(x => x.Email.Equals(email)).FirstAsync();
            return await _unitOfWork.User.GetAll().Where(x => x.ScienceArea.Equals(user.ScienceArea))
                .Where(x => x.Email != email && x.Email != originalEmail).ToListAsync();
        }

        public async Task<IEnumerable<User>> SearchUsers(string searchString)
        {
            return await _unitOfWork.User.GetAll().Where(x => x.FirstName.Contains(searchString) || x.LastName.Contains(searchString)).ToListAsync();
        }

        public async Task<User> GetCurrentUser(string email)
        {
            return await _unitOfWork.User.GetAll().Where(x => x.Email.Equals(email)).FirstAsync();
        }

        public void UpdateUser(User user, string email)
        {
            var temp = _unitOfWork.User.GetAll().Where(x => x.Email.Equals(email)).FirstAsync();
            user.UserId = temp.Result.UserId;
            user.Created = temp.Result.Created;
            user.Age = temp.Result.Age;
            user.Posts = temp.Result.Posts;

            _unitOfWork.User.Update(user, temp.Result);
            _unitOfWork.Complete();
        }
    }
}
﻿using System.Web.Helpers;
using EntityLayer.Models;
using DataLayer.UnitOfWork;

namespace BusinessLayer.Controllers
{
    public class SignInController
    {
        private readonly IUnitOfWork _unitOfWork;

        public SignInController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<bool> SignIn(string email, string enteredPassword)
        {
            var users =
                _unitOfWork.User.Find(x => x.Email.Equals(email));

            if (!ValidatePassword(users.FirstAsync().Result.Password, enteredPassword))
                return false;

            return await users.AnyAsync();
        }

        private static bool ValidatePassword(string hash, string enteredPassword)
        {
            return Crypto.VerifyHashedPassword(hash, enteredPassword);
        }
    }
}

using EntityLayer.Models;
using Newtonsoft.Json;

namespace BusinessLayer.Controllers
{
    public class CountriesController
    {
        public async Task<List<Country>> GetCountries()
        {
            using (var client = new HttpClient())
            {
                var apiResponse = await client.GetAsync("https://restcountries.com/v2/all");

                if (apiResponse.IsSuccessStatusCode)
                {
                    var countryResponse = apiResponse.Content.ReadAsStringAsync().Result;
                    return JsonConvert.DeserializeObject<List<Country>>(countryResponse);
                }
            }

            return null;
        }
    }
}
using EntityLayer.Models;
using Microsoft.EntityFrameworkCore;
using DataLayer.UnitOfWork;

namespace BusinessLayer.Controllers
{
    public class PostController
    {
        private readonly IUnitOfWork _unitOfWork;

        public PostController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void CreatePost(string title, string content, string email, DateTime created) 
        {
            var user = _unitOfWork.User.Find(x => x.Email.Equals(email)).FirstAsync();

            var post = new Post
            {
                Title = title,
                Content = content,
                User = user.Result,
                CreatedDate = created,
            };

            _unitOfWork.Post.Add(post);
            _unitOfWork.Complete();
        }

        public async Task<IEnumerable<Post>> GetAllPost(string email) 
        {
            var currentUser = await _unitOfWork.User.Find(x => x.Email.Equals(email)).FirstAsync();
            var users = await _unitOfWork.User.GetAll().ToListAsync();
            var posts = await _unitOfWork.Post.GetAll().ToListAsync();

            foreach (var user in users)
            {
                foreach (var post in posts)
                {
                    if (user.UserId.Equals(post.UserId))
                    {
                        post.User = user;
                    }
                }
            }

            return await _unitOfWork.Post.GetAll().Where(x => x.User.ScienceArea.Equals(currentUser.ScienceArea)).ToListAsync();
        }

        public async Task<IEnumerable<Post>> GetCurrentUserPosts(string email)
        {
            var user = await _unitOfWork.User.GetAll().Where(x => x.Email.Equals(email)).FirstAsync();
            return await _unitOfWork.Post.GetAll().Where(x => x.UserId.Equals(user.UserId)).ToListAsync();
        }
    }
}
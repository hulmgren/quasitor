using Newtonsoft.Json;
using EntityLayer.Models;

namespace BusinessLayer.Controllers
{
    public class UniversityController
    {
        public async Task<List<University>> GetUniversities()
        {
            using (var client = new HttpClient())
            {
                var apiResponse = await client.GetAsync("https://raw.githubusercontent.com/Hipo/university-domains-list/master/world_universities_and_domains.json");

                if (apiResponse.IsSuccessStatusCode)
                {
                    var universityResponse = apiResponse.Content.ReadAsStringAsync().Result;
                    return JsonConvert.DeserializeObject<List<University>>(universityResponse);
                }
            }

            return null;
        }
    }
}

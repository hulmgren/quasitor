﻿using DataLayer.UnitOfWork;

namespace BusinessLayer.Controllers
{
    public class BaseController
    {
        protected readonly IUnitOfWork UnitOfWork;

        public BaseController(IUnitOfWork unitOfWork)
        {
            UnitOfWork = unitOfWork;
        }
    }
}

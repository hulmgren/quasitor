﻿using EntityLayer.Models;
using Microsoft.AspNetCore.Mvc;
using PresentationLayer.Models;
using BusinessLayer.Controllers;
using AutoMapper;

namespace PresentationLayer.Controllers
{
    public class SignUpController : Controller
    {
        private readonly BusinessLayer.Controllers.SignUpController _signUpController;
        private readonly CountriesController _countriesController;
        private readonly UniversityController _universityController;
        private readonly IMapper _mapper;

        public SignUpController(BusinessLayer.Controllers.SignUpController signUpController,
            CountriesController countriesController, IMapper mapper, UniversityController universityController)
        {
            _signUpController = signUpController;
            _countriesController = countriesController;
            _universityController = universityController;
            _mapper = mapper;
        }

        [Route("signup")]
        public async Task<IActionResult> Index()
        {
            var model = new SignUpViewModel();
            model.Countries = new List<PresentationLayer.Models.Country>();
            model.Universities = new List<Models.University>();

            var countries = await _countriesController.GetCountries();
            var universities = await _universityController.GetUniversities();

            foreach (var country in countries)
                model.Countries.Add(_mapper.Map<EntityLayer.Models.Country, PresentationLayer.Models.Country>(country));

            foreach (var uni in universities)
                model.Universities.Add(_mapper.Map<EntityLayer.Models.University, PresentationLayer.Models.University>(uni));
            
            model.Universities = model.Universities.OrderByDescending(x => x.name).ToList();

            return View(model);
        }

        [HttpPost]
        [Route("process")]
        public async Task<IActionResult> SignUp(SignUpViewModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var user = _mapper.Map<SignUpViewModel, User>(model);
            user.Picture = await GetBytesAsync(model.Picture);
            
            try
            {
                await _signUpController.CreateAccount(user);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return RedirectToAction("Index", "SignIn");
        }

        private async Task<byte[]> GetBytesAsync(IFormFile formFile)
        {
            if (formFile is not null)
            {
                using var ms = new MemoryStream();
                await formFile.CopyToAsync(ms);
                return ms.ToArray();
            }
            
            return System.IO.File.ReadAllBytes("./wwwroot/lib/defaultProfilePic.jpeg");
        }
    }
}

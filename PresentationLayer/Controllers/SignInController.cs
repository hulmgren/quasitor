﻿using System.Security.Claims;
using BusinessLayer.Controllers;
using Microsoft.AspNetCore.Mvc;
using PresentationLayer.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace PresentationLayer.Controllers
{
    public class SignInController : Controller
    {
        private readonly BusinessLayer.Controllers.SignInController _signInController;

        public SignInController(BusinessLayer.Controllers.SignInController signInController) => 
            _signInController = signInController;

        public IActionResult Index()
        {
            if (HttpContext.User.Identity is {IsAuthenticated: true})
                return RedirectToAction("Index", "Home");

            return View();
        }

        public IActionResult Modal() 
        {
            return View("Modal");
        }

        [HttpPost]
        public async Task<ActionResult> SignIn(SignInViewModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            if (await _signInController.SignIn(model.Email, model.Password))
                await AuthCustomer(model);
            else
                return NotFound();

            return RedirectToAction("Index", "Home");
        }

        private Task AuthCustomer(SignInViewModel model)
        {
            var identity = new ClaimsIdentity(new[] { new Claim(ClaimTypes.Name, model.Email) }, CookieAuthenticationDefaults.AuthenticationScheme);
            var principle = new ClaimsPrincipal(identity);
            return HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principle);
        }
    }
}

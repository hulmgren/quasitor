using Microsoft.AspNetCore.Mvc;
using PresentationLayer.Models;
using System.Diagnostics;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using BusinessLayer.Controllers;
using EntityLayer.Models;
using AutoMapper;

namespace PresentationLayer.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly UserController _userController;
        private readonly CountriesController _countriesController;
        private readonly PostController _postController;
        private readonly UniversityController _universityController;
        private readonly IMapper _mapper;

        public AccountController(ILogger<HomeController> logger, IMapper mapper,
            UserController userController, CountriesController countriesController,
            PostController postController, UniversityController universityController)
        {
            _logger = logger;
            _userController = userController;
            _countriesController = countriesController;
            _postController = postController;
            _universityController = universityController;
            _mapper = mapper;
        }

        [HttpGet]
        [Route("edit")]
        public async Task<IActionResult> Index()
        {
            return View(await GetUser());
        }

        [HttpGet]
        [Route("account")]
        public async Task<IActionResult> ViewAccount()
        {
            return View(await GetUserWithSuggestions());
        }

        [HttpPut]
        public async Task<JsonResult> Update([FromBody] AccountViewModel model)
        {
            try
            {
                var user = _mapper.Map<AccountViewModel, User>(model);

                _userController.UpdateUser(user, HttpContext.User.Identity.Name);
            }
            catch (Exception ex)
            {
                return Json(new { result = ex.Message });
            }

            return Json(new { result = "Success" });
        }

        private async Task<AccountViewModel> GetUserWithSuggestions()
        {
            var rnd = new Random();
            var model = new AccountViewModel();
            model = await GetUser();
            model.Users = await GetSuggestions(model, rnd);
            model.Posts = await GetPosts(model);

            return model;
        }

        private async Task<List<Feed>> GetPosts(AccountViewModel model)
        {
            model.Posts = new List<Feed>();
            var posts = await _postController.GetCurrentUserPosts(HttpContext.User.Identity.Name);

            foreach (var post in posts)
                model.Posts.Add(_mapper.Map<Post, Feed>(post));

            model.Posts = model.Posts.OrderByDescending(x => x.Created).ToList();
            return model.Posts;
        }

        private async Task<List<Users>> GetSuggestions(AccountViewModel model, Random rnd)
        {
            model.Users = new List<Users>();
            var users = await _userController.GetSuggestions(HttpContext.User.Identity.Name, string.Empty);

            foreach (var user in users)
                model.Users.Add(_mapper.Map<User, Users>(user));

            if (model.Users.Count > 3)
                model.Users = model.Users.OrderBy(x => rnd.Next()).Take(4).ToList();
            else
                model.Users = model.Users.OrderBy(x => rnd.Next()).Take(model.Users.Count).ToList();

            return model.Users;
        }

        private async Task<AccountViewModel> GetUser()
        {
            var model = new AccountViewModel();
            var user = await _userController.GetCurrentUser(HttpContext.User.Identity.Name);

            model = _mapper.Map<User, AccountViewModel>(user);

            await GetCountries(model);
            await GetUniversities(model);

            return model;
        }

        private async Task<AccountViewModel> GetCountries(AccountViewModel model)
        {
            model.Countries = new List<Models.Country>();
            var countries = await _countriesController.GetCountries();

            foreach (var country in countries)
                model.Countries.Add(_mapper.Map<EntityLayer.Models.Country, PresentationLayer.Models.Country>(country));
            
            return model;
        }

        private async Task<AccountViewModel> GetUniversities(AccountViewModel model)
        {
            model.Universities = new List<Models.University>();
            var universities = await _universityController.GetUniversities();

            foreach (var uni in universities)
                model.Universities.Add(_mapper.Map<EntityLayer.Models.University, PresentationLayer.Models.University>(uni));
            
            return model;
        }

        private async Task<byte[]> GetBytesAsync(IFormFile formFile)
        {
            if (formFile is not null)
            {
                using var ms = new MemoryStream();
                await formFile.CopyToAsync(ms);
                return ms.ToArray();
            }
            
            return System.IO.File.ReadAllBytes("./wwwroot/lib/defaultProfilePic.jpeg");
        }
    }
}
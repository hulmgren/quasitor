using Microsoft.AspNetCore.Mvc;
using PresentationLayer.Models;
using Microsoft.AspNetCore.Authorization;
using BusinessLayer.Controllers;
using EntityLayer.Models;
using AutoMapper;

namespace PresentationLayer.Controllers
{
    [Authorize]
    public class ProfileController : Controller
    {
        private readonly UserController _userController;
        private readonly PostController _postController;
        private readonly IMapper _mapper;

        public ProfileController(UserController userController, IMapper mapper, PostController postController)
        {
            _userController = userController;
            _postController = postController;
            _mapper = mapper;
        }

        public async Task<IActionResult> Index(ProfileViewModel model)
        {
            if (model.Email != HttpContext.User.Identity.Name)
                return View(await GetUser(model));
            
            return RedirectToAction("ViewAccount", "Account");
        }

        private async Task<ProfileViewModel> GetUser(ProfileViewModel model)
        {
            var rnd = new Random();
            var newModel = new ProfileViewModel();
            var user = await _userController.GetCurrentUser(model.Email);

            newModel = _mapper.Map<User, ProfileViewModel>(user);
            await GetSuggestions(model.Email, newModel, rnd);
            await GetPosts(model.Email, newModel);
            
            return newModel;
        }

        private async Task<List<Feed>> GetPosts(string email, ProfileViewModel model)
        {
            model.Posts = new List<Feed>();
            var posts = await _postController.GetCurrentUserPosts(email);

            foreach (var post in posts)
                model.Posts.Add(_mapper.Map<EntityLayer.Models.Post, Feed>(post));

            return model.Posts = model.Posts.OrderByDescending(x => x.Created).ToList();
        }

        private async Task<List<Users>> GetSuggestions(string email, ProfileViewModel model, Random rnd)
        {
            model.Users = new List<Users>();
            var users = await _userController.GetSuggestions(email, HttpContext.User.Identity.Name);

            foreach (var user in users)
                model.Users.Add(_mapper.Map<User, Users>(user));

            if (model.Users.Count > 3)
                return model.Users = model.Users.OrderBy(x => rnd.Next()).Take(4).ToList();
            
            return model.Users = model.Users.OrderBy(x => rnd.Next()).Take(model.Users.Count).ToList();
        }
    }
}
﻿using Microsoft.AspNetCore.Mvc;
using PresentationLayer.Models;
using System.Diagnostics;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using BusinessLayer.Controllers;
using EntityLayer.Models;
using AutoMapper;
using Microsoft.AspNetCore.Http;

namespace PresentationLayer.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly PostController _postController;
        private readonly UserController _userController;
        private readonly IMapper _mapper;

        public HomeController(ILogger<HomeController> logger, IMapper mapper,
            PostController postController, UserController userController)
        {
            _logger = logger;
            _postController = postController;
            _userController = userController;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            return View(await GetAllPostsAndUsers());
        }

        [Route("about")]
        public IActionResult About()
        {
            return View("About");
        }

        [Route("signout")]
        public async Task<IActionResult> SignOutAction() 
        {
            await CustomSignOut();

            return RedirectToAction("Index", "SignIn");
        }

        [HttpPost]
        [Route("post")]
        public async Task<IActionResult> CreatePost(Feed post) 
        {
            post.User = HttpContext.User.Identity.Name;
            post.Created = DateTime.Now;

            var insert = InsertPost(post);
            var header = Request.GetTypedHeaders();
            var uri = header.Referer;

            return new RedirectResult(uri.AbsoluteUri);
        }

        [HttpPost]
        public async Task<JsonResult> SearchUsers([FromBody] string searchString)
        {
            var model = new List<Search>();
            if (!string.IsNullOrEmpty(searchString))
            {
                var users = await _userController.SearchUsers(searchString);

                foreach (var user in users)
                    model.Add(_mapper.Map<User, Search>(user));

                return Json(model.Take(model.Count).Distinct()); // Change to 5 later
            }
            return null;
        }

        private async Task InsertPost(Feed post)
        {
            _postController.CreatePost(post.Title, post.Content, post.User, post.Created);
        }

        private async Task<List<Users>> GetSuggestions(string email, HomeViewModel model, Random rnd)
        {
            model.Users = new List<Users>();
            var users = await _userController.GetSuggestions(email, string.Empty);

            foreach (var user in users)
                model.Users.Add(_mapper.Map<User, Users>(user));

            if (model.Users.Count > 3)
                return model.Users = model.Users.OrderBy(x => rnd.Next()).Take(4).ToList();
            
            return model.Users = model.Users.OrderBy(x => rnd.Next()).Take(model.Users.Count).ToList();
        }

        private async Task<List<Feed>> GetPosts(string email, HomeViewModel model)
        {
            model.Feed = new List<Feed>();
            var posts = await _postController.GetAllPost(email);

            foreach (var post in posts)
                model.Feed.Add(_mapper.Map<EntityLayer.Models.Post, Feed>(post));

            return model.Feed = model.Feed.OrderByDescending(x => x.Created).ToList();
        }

        // // private async Task<List<Search>> SearchUsers(HomeViewModel model)
        // // {
        // //     model.Search = new List<Search>();
        // //     var users = await _userController.SearchUsers();

        // //     foreach (var user in users)
        // //         model.Search.Add(_mapper.Map<User, Search>(user));

        // //     return model.Search;
        // // }

        private async Task<HomeViewModel> GetAllPostsAndUsers()
        {
            var rnd = new Random();
            var user = HttpContext.User.Identity.Name;
            var model = new HomeViewModel();

            await GetPosts(user, model);

            // await SearchUsers(model);
            
            await GetSuggestions(user, model, rnd);

            return model;
        }

        private async Task CustomSignOut() 
        {
            if (HttpContext.Request.Cookies.Count> 0) 
            {
                var siteCookies = HttpContext.Request.Cookies.Where(c => c.Key.Contains(".AspNetCore.") || c.Key.Contains("Microsoft.Authentication"));
                foreach (var cookie in siteCookies)
                {
                    Response.Cookies.Delete(cookie.Key);
                }
            }

            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            HttpContext.Session.Clear();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
using AutoMapper;
using PresentationLayer.Models;
using EntityLayer.Models;

namespace PresentationLayer.Mapper
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<User, Users>()
                .ForMember(dest => dest.DecryptedPicture, opt => opt.Ignore());
            
            CreateMap<EntityLayer.Models.Post, Feed>()
                .ForMember(dest => dest.Created, opt => opt.MapFrom(x => x.CreatedDate))
                .ForMember(dest => dest.Picture, opt => opt.MapFrom(x => x.User.Picture))
                .ForMember(dest => dest.ScienceArea, opt => opt.MapFrom(x => x.User.ScienceArea))
                .ForMember(dest => dest.DecryptedPicture, opt => opt.Ignore())
                .ForMember(dest => dest.User, opt => opt.MapFrom(x => x.User.FirstName + ' ' + x.User.LastName))
                .ForMember(dest => dest.Email, opt => opt.MapFrom(x => x.User.Email));

            CreateMap<SignUpViewModel, User>()
                .ForMember(dest => dest.Picture, opt => opt.MapFrom(x => x.BytePicture))
                .ForMember(dest => dest.Age, opt => opt.MapFrom(x => DateTime.Now.Year - Convert.ToInt32(x.Age.Substring(0, 4))))
                .ForMember(dest => dest.UserId, opt => opt.Ignore())
                .ForMember(dest => dest.Created, opt => opt.Ignore())
                .ForMember(dest => dest.Posts, opt => opt.Ignore());

            CreateMap<User, AccountViewModel>()
                .ForMember(dest => dest.BytePicture, opt => opt.MapFrom(x => x.Picture))
                .ForMember(dest => dest.ConfirmedPassword, opt => opt.Ignore())
                .ForMember(dest => dest.Countries, opt => opt.Ignore())
                .ForMember(dest => dest.Picture, opt => opt.Ignore())
                .ForMember(dest => dest.Users, opt => opt.Ignore())
                .ForMember(dest => dest.Universities, opt => opt.Ignore());
            
            CreateMap<AccountViewModel, User>()
                .ForMember(dest => dest.UserId, opt => opt.Ignore())
                .ForMember(dest => dest.Created, opt => opt.Ignore())
                .ForMember(dest => dest.Posts, opt => opt.Ignore())
                .ForMember(dest => dest.Picture, opt => opt.Ignore());

            CreateMap<EntityLayer.Models.Country, PresentationLayer.Models.Country>();

            CreateMap<EntityLayer.Models.University, PresentationLayer.Models.University>();

            CreateMap<User, Search>()
                .ForMember(dest => dest.Name, opt => opt.MapFrom(x => $"{x.FirstName} {x.LastName}"));
            
            CreateMap<User, ProfileViewModel>()
                .ForMember(dest => dest.DecryptedPicture, opt => opt.Ignore())
                .ForMember(dest => dest.Users, opt => opt.Ignore());
        }
    }
}
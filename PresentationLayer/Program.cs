using System.Configuration;
using EntityLayer.AppDbContext;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.SqlServer;
using AutoMapper;
using PresentationLayer.Mapper;
using DataLayer.UnitOfWork;
using BusinessLayer.Controllers;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();
builder.Services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie();
builder.Services.AddSession();

var configuration = new MapperConfiguration(cfg => 
{
    cfg.AddProfile<MapperProfile>();
});

configuration.CreateMapper();
configuration.AssertConfigurationIsValid();

builder.Services.AddAutoMapper(typeof(Program));
builder.Services.AddMvc();

builder.Services.AddDbContext<AppDbContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("QuasitorDBConnection")), ServiceLifetime.Transient);

builder.Services.AddTransient<IUnitOfWork, UnitOfWork>();
builder.Services.AddTransient<UserController>();
builder.Services.AddTransient<CountriesController>();
builder.Services.AddTransient<PostController>();
builder.Services.AddTransient<SignInController>();
builder.Services.AddTransient<SignUpController>();
builder.Services.AddTransient<UniversityController>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthentication();

app.UseAuthorization();

app.UseSession();  

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=SignIn}/{action=Index}/{id?}");

app.Run();

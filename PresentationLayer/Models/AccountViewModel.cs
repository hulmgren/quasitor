namespace PresentationLayer.Models
{
    public class AccountViewModel : SignUpViewModel
    {
        public List<Users> Users { get; set; }
        public List<Feed> Posts { get; set; }
    }
}
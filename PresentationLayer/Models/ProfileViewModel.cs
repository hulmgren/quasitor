namespace PresentationLayer.Models
{
    public class ProfileViewModel
    {
        public string Email { get; set; }
        public string University { get; set; }
        public string ScienceArea { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Country { get; set; }
        public byte[] Picture { get; set; }
        public string DecryptedPicture { get; set; }
        public List<Feed> Posts { get; set; }
        public List<Users> Users { get; set; }
    }
}
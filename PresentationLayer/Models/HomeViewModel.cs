using System.ComponentModel.DataAnnotations;

namespace PresentationLayer.Models
{
    public class HomeViewModel
    {
        public List<Feed>? Feed { get; set; }
        public List<Users>? Users { get; set; }
    }

    public class Feed
    {
        [Required]
        public string Title { get; set; } 
        [Required]
        public string Content { get; set; }
        public DateTime Created { get; set; }
        public string User { get; set; }
        public byte[]? Picture { get; set; }
        public string ScienceArea { get; set; }
        public string DecryptedPicture { get; set; }
        public string Email { get; set; }
    }

    public class Users
    {
        public string Email { get; set; }
        public string University { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Country { get; set; }
        public string ScienceArea { get; set; }
        public byte[] Picture { get; set; }
        public string DecryptedPicture { get; set; }
    }

    public class Search 
    {
        public string Name { get; set; }
        public string Email { get; set; }
    }
}
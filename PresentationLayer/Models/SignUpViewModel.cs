﻿using System.ComponentModel.DataAnnotations;

namespace PresentationLayer.Models
{
    public class SignUpViewModel
    {
        [Required]
        [EmailAddress(ErrorMessage = "Enter a valid email address.")]
        public string Email { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string University { get; set; }
        [Required]
        public string Country { get; set; }
        [Required]
        public string ScienceArea { get; set; }
        [Required]
        public string Age { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        [Compare("Password", ErrorMessage = "Password does not match.")]
        public string ConfirmedPassword { get; set; }
        public IFormFile? Picture { get; set; } 
        public byte[]? BytePicture { get; set; }
        public List<Country>? Countries { get; set; }
        public List<University>? Universities { get; set; }
    }

    public class Country 
    {
        public string name { get; set; }
    }

    public class University
    {
        public string name { get; set; }
    }
}

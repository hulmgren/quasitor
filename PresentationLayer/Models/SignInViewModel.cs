﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace PresentationLayer.Models
{
    public class SignInViewModel
    {
        [Required]
        [EmailAddress(ErrorMessage = "Please enter a valid email.")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Email or password is incorrect")]
        [PasswordPropertyText]
        public string Password { get; set; }
    }
}

﻿$("input[name='email']").focus(function(){
    $(this).closest('div').addClass('focused');
});

$("input[name='email']").blur(function(){
    var inputValue = $(this).val();
    if ( inputValue == "" ) {
        $(this).closest('div').removeClass('focused');  
    } else {
        $(this).addClass('filled');
    }
})  

$("input[name='password']").focus(function(){
    $(this).closest('div').addClass('focused');
});

$("input[name='password']").blur(function(){
    var inputValue = $(this).val();
    if ( inputValue == "" ) {
        $(this).closest('div').removeClass('focused');  
    } else {
        $(this).addClass('filled');
    }
})  
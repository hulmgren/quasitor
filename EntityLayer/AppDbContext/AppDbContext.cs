﻿using EntityLayer.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace EntityLayer.AppDbContext
{
    public class AppDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Post> Posts { get; set; }

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
            
        }

        // protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        // {
        //     var configuration = new ConfigurationBuilder()
        //         .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
        //         .AddJsonFile("/Users/hugobackmanulmgren/Quasitor/quasitor/PresentationLayer/appsettings.json")
        //         .Build();

        //     optionsBuilder.UseSqlServer(configuration.GetConnectionString("QuasitorDBConnection"));
        // }
    }
}

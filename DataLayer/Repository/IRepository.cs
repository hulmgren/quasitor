﻿using System.Linq.Expressions;

namespace DataLayer.Repository
{
    public interface IRepository<T> where T : class
    {
        /// <summary>
        /// Creates an <see cref="entity"/> in the database
        /// </summary>
        /// <param name="entity"></param>
        void Add(T entity);
        
        /// <summary>
        /// Creates many <see cref="entities"/> in the database
        /// </summary>
        /// <param name="entities"></param>
        void AddRange(IEnumerable<T> entities);

        /// <summary>
        /// Searches for entity in the database based on <see cref="predicate"/>
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        
        IAsyncEnumerable<T> Find(Expression<Func<T, bool>> predicate);
        
        /// <summary>
        /// Gets a entity from the database based on <see cref="id"/>
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        T Get(int id);

        /// <summary>
        /// Gets all entities from the database
        /// </summary>
        /// <returns></returns>
        IAsyncEnumerable<T> GetAll();

        /// <summary>
        /// Deletes an <see cref="entity"/> from the database
        /// </summary>
        /// <param name="entity"></param>
        void Remove(T entity);

        /// <summary>
        /// Deletes many <see cref="entities"/> from the database
        /// </summary>
        /// <param name="entities"></param>
        void RemoveRange(IEnumerable<T> entities);

        void Update(T entity, T getEntity);
    }
}

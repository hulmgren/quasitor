﻿using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace DataLayer.Repository
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private readonly DbContext _dbContext;

        public Repository(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void Add(TEntity entity)
        {
            _dbContext.Set<TEntity>().Add(entity);
        }

        public void AddRange(IEnumerable<TEntity> entities)
        {
            _dbContext.Set<TEntity>().AddRange(entities);
        }

        public IAsyncEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbContext.Set<TEntity>().Where(predicate).AsAsyncEnumerable();
        }

        public TEntity Get(int id)
        {
            return _dbContext.Set<TEntity>().Find(id);
        }

        public IAsyncEnumerable<TEntity> GetAll()
        {
            return _dbContext.Set<TEntity>().AsAsyncEnumerable();
        }

        public void Remove(TEntity entity)
        {
            _dbContext.Set<TEntity>().Remove(entity);
        }

        public void RemoveRange(IEnumerable<TEntity> entities)
        {
            _dbContext.Set<TEntity>().RemoveRange(entities);
        }

        public void Update(TEntity entity, TEntity getEntity)
        {
            var entry = _dbContext.Entry(entity);
            
            foreach (var property in typeof(TEntity).GetProperties())
            {
                if (property.GetValue(entity, null) == null || property.GetValue(entity, null).Equals(string.Empty))
                {
                    if (property.PropertyType != typeof(string) && typeof(IEnumerable<object>).IsAssignableFrom(property.PropertyType))
                        entry.Collection(property.Name).CurrentValue = (IEnumerable<object>)property.GetValue(getEntity);
                    else
                        entry.Property(property.Name).CurrentValue = property.GetValue(getEntity);
                }
                else
                    property.SetValue(getEntity, entry.Property(property.Name).CurrentValue);
            }

            entry.CurrentValues.SetValues(getEntity);
        }
    }
}

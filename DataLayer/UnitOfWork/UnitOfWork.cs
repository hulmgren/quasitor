﻿using DataLayer.Repository;
using EntityLayer.Models;
using EntityLayer.AppDbContext;

namespace DataLayer.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _appDbContext;

        public IRepository<User> User { get; }
        public IRepository<Post> Post { get; }

        public UnitOfWork(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
            User = new Repository<User>(_appDbContext);
            Post = new Repository<Post>(_appDbContext);
        }

        public int Complete() => _appDbContext.SaveChanges();

        public void Dispose() => _appDbContext.Dispose();
    }
}

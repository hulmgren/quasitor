﻿using DataLayer.Repository;
using EntityLayer.Models;

namespace DataLayer.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<User> User { get; }
        IRepository<Post> Post { get; }
        int Complete();
    }
}
